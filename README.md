# Esp-Update
This repo contains compiled binaries, file system images and their version numbers for the very specific lamp.

Firmware bin is obtained using IDE using "Export compiled binary"

Filesystem bin is obtained by using ESP8266 LittleFS tool while the ESP is *not* connected. The 1.8 IDE will fail uploading the image, but the image will still be created. There will be a path to the created binary somewhere in the log. Copy this image to this repository.

In order to correctly update firmware, all following must be done:
	Change the version number in lamp.ino
	!check the board version definition!
	Change the version number in fwX_X_ver.txt

In order to correctly update filesystem, all following must be done:
	Change the version number in data/FS_version.txt
	check the board version definition, but both FSs should be same
	Change the version number in lfsX_X_ver.txt